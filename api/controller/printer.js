/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const DefaultStyles = {
	italic: { italics: true },
	bold: { bold: true },
	strikethrough: { decoration: "lineThrough" },
	link: { color: "#6688ff" },
	sup: { fontSize: 8 },
	sub: { fontSize: 8 },
};

module.exports = function() {
	/** @var {HitchyAPI} api */
	const api = this;
	const { services } = api.runtime;

	/**
	 * Implements request handlers for generating PDF documents from provided
	 * input.
	 */
	class PrinterController {
		/**
		 * Generates PDF document from markdown provided in JSON-formatted
		 * request body.
		 *
		 * @param {HitchyIncomingMessage} request request descriptor
		 * @param {HitchyServerResponse} response response manager
		 * @returns {Promise} promises response
		 */
		static pdf( request, response ) {
			return request.fetchBody()
				.then( body => {
					const state = body || {};

					if ( !body && !request.query.content ) {
						throw new services.HttpException( 400, "missing request body" );
					}

					if ( process.env.NODE_ENV !== "production" && request.query.content ) {
						state.content = request.query.content;
					}

					const { content, theme = {} } = state;
					let parsedTheme = theme;

					if ( typeof theme === "string" ) {
						try {
							parsedTheme = JSON.parse( theme ) || {};
						} catch ( error ) {
							throw new services.HttpException( 400, "invalid theme definition: " + error.message );
						}
					}

					if ( !content ) {
						throw new services.HttpException( 400, "missing content to convert" );
					}

					parsedTheme.styles = Object.assign( {}, DefaultStyles, parsedTheme.styles || {} );

					const pdf = services.MarkdownToPdf.convert( content, parsedTheme, {
						typographer: request.query.typographer !== "0",
						title: request.query.title || undefined,
						protect: request.query.protect,
						dumpParsed: ( request.query.dump || body.dump ) === "parsed",
						dumpRendered: ( request.query.dump || body.dump ) === "rendered",
						rng: count => require( "crypto" ).randomBytes( count ),
					} );

					if ( typeof pdf.pipe === "function" ) {
						response
							.set( "content-type", "application/pdf" );

						if ( request.query.inline ) {
							response.set( "content-disposition", "inline; filename=" + request.params.filename );
						}

						pdf.pipe( response );
						pdf.end();
					} else {
						response
							.set( "content-type", "application/json" )
							.send( JSON.stringify( pdf, null, 4 ) );
					}
				} )
				.catch( error => {
					return response
						.status( error.statusCode || 500 )
						.json( { error: process.env.NODE_ENV === "production" ? error.message : error.stack } );
				} );
		}
	}

	return PrinterController;
};

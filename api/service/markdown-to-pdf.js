/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { process: processMarkdown } = require( "../../lib/markdown" );
const { renderTokens } = require( "../../lib/pdf" );


module.exports = function() {
	const api = this;
	const logWarning = api.log( "pdf-maker:converter:warning" );


	/**
	 * Implements conversion of markdown to PDF.
	 */
	class MarkdownToPdf {
		/**
		 * Returns stream producing PDF document containing provided markdown
		 * input.
		 *
		 * @param {string} input markdown-formatted input
		 * @param {Theme} theme definition of resulting document's theme
		 * @param {object} options options customizing conversion process
		 * @returns {ReadableStream|object} stream producing PDF document or content definition for dumping
		 */
		static convert( input, theme = {}, options = {} ) {
			const { state, tokens } = processMarkdown( input, Object.assign( {
				quotes: "„“‚‘",
			}, options, {
				render: false,
			} ) );

			if ( options.dumpParsed ) {
				return tokens;
			}

			return renderTokens( tokens, state, Object.assign( {
				theme,
				logWarning,
				fonts: {
					Roboto: {
						normal: "assets/fonts/Roboto-Regular.ttf",
						bold: "assets/fonts/Roboto-Medium.ttf",
						italics: "assets/fonts/Roboto-Italic.ttf",
						bolditalics: "assets/fonts/Roboto-MediumItalic.ttf",
					}
				},
			}, options ) );
		}
	}

	return MarkdownToPdf;
};

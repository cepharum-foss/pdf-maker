# PDF Maker

converting Markdown to PDF

## License

[MIT](LICENSE)

## About

This tool is combining [markdown-it](https://www.npmjs.com/package/markdown-it) with [pdfmake](https://www.npmjs.com/package/pdfmake) using [Hitchy](https://www.npmjs.com/package/@hitchy/core) to expose a markdown-to-PDF converter via REST-like API.

## API

When exposing this service at http://127.0.0.1:3000, sending a **POST** request to 

http://127.0.0.1:3000/print/test.pdf 

is generating PDF document named **test.pdf**. The resulting document isn't stored on server, but instantly sent to requesting client in response.

The request needs to provide content to be rendered. Provide information in JSON-encoded request body describing with property `content` providing content of resulting document as Markdown-encoded string. In addition, a second property named `theme` is optionally providing styling information mostly forwarded to **pdfmake**. See the following example:

```json
{
    "content": "# Title\n\nThis is some Markdown text including:\n\n* a list\n* with two items.",
    "theme": {
         "defaultStyle": {
             "fontSize": 11
         },
         "pageSize": "A4",
         "styles": {
             "p": {
                 "marginTop": 11
             },
             "ul": {
                 "marginTop": 11
             },
             "ol": {
                 "marginTop": 11
             },
             "h1": {
                 "marginTop": 36,
                 "fontSize": 36
             },
             "h2": {
                 "fontSize": 24,
                 "marginTop": 10
             },
             "h3": {
                 "fontSize": 20,
                 "bold": true,
                 "italics": true,
                 "marginTop": 10
             }
         }
    }
}
```

## How it works

By combining markdown-it with pdfmake, this application is parsing some provided markdown text converting resulting hierarchy of tokens into content definition suitable for use with pdfmake. In addition, an extended engine is processing styling definitions in provided theme injecting  pdfmake-compatible styling definitions as well as adjusting resulting content definitions.

## Demonstration

The service is available as a docker container.

```
docker run --rm -p 3000:3000  cepharum/pdf-maker
```

This command is running pdf-maker. When started, open [http://127.0.0.1:3000](http://127.0.0.1:3000) in your browser. This will load a simple page providing editor for generating PDF documents from markdown content and JSON styling.

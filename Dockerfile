FROM node:lts-alpine AS build

WORKDIR /app

COPY . /app
RUN npm ci

CMD ["npm", "run", "start", "--", "--ip", "0.0.0.0"]

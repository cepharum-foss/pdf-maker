/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";


const PdfMake = require( "pdfmake" );



const DefaultContent = {
	hardbreak: "\n",
	softbreak: " ",
};

const DefaultStyle = {
	markerColor: "black",
};

/**
 * Helps discovering style properties that cause extra box rendered around some
 * styled content.
 *
 * @type {object<string, true>}
 */
const BoxingProperty = {
	borderTop: true,
	borderRight: true,
	borderBottom: true,
	borderLeft: true,
	borderTopColor: true,
	borderRightColor: true,
	borderBottomColor: true,
	borderLeftColor: true,
	borderColor: true,
	borderTopStyle: true,
	borderRightStyle: true,
	borderBottomStyle: true,
	borderLeftStyle: true,
	borderStyle: true,
	backgroundColor: true,
	columns: true,
};

const BaseMap = {
	marginTop: "margin",
	marginRight: "margin",
	marginBottom: "margin",
	marginLeft: "margin",
	borderTop: "border",
	borderRight: "border",
	borderBottom: "border",
	borderLeft: "border",
	paddingTop: "padding",
	paddingRight: "padding",
	paddingBottom: "padding",
	paddingLeft: "padding",
};

const DirectionNames = [ "Top", "Right", "Bottom", "Left" ];

/**
 * Tracks numeric ID of next image to collect in theme for caching multiple
 * occurrences.
 *
 * @type {number}
 */
let indexImage = 1;



/**
 * Implements common processing of markdown input.
 *
 * @note This method has been extracted to expose internally used markdown
 *       parser to consuming libraries for custom use.
 *
 * @param {*} tokenList parsed tokens of markdown document to be rendered
 * @param {{stack: [], inline: {}, block: [], env: {}}} context context of parsing and rendering document
 * @param {TokenRendererOptions} options customizations
 * @returns {ReadableStream|object} stream producing PDF document or content definition for dumping
 */
exports.renderTokens = ( tokenList, context, options = {} ) => {
	const {
		theme: customTheme,
		logWarning = () => {},  // eslint-disable-line no-empty-function
	} = options;

	const pdfDescription = {
		info: {
			creator: "@cepharum/pdf-maker",
			title: options.title,
		},
		content: convertBlock( context, customTheme, {
			tokens: tokenList,
			next: 0,
			length: tokenList.length
		} ),
	};

	if ( customTheme ) {
		for ( const name of [ "styles", "defaultStyle", "header",
		                      "footer", "pageSize", "pageOrientation",
		                      "pageMargins", "images" ] ) {
			const source = name === "styles" ? "compiled" : name;

			if ( customTheme.hasOwnProperty( source ) ) {
				switch ( name ) {
					case "header" :
					case "footer" : {
						const original = customTheme[source];

						pdfDescription[name] = ( currentPage, pageCount /* , pageSize */ ) => {
							return processEveryText( original, node => {
								return Object.assign( {}, node, {
									text: node.text.replace( /{([^}]+)}/g, ( _, id ) => {
										switch ( id.trim().toLowerCase() ) {
											case "current" :
											case "currentpage" :
											case "page" :
											case "no" :
												return currentPage;

											case "pages" :
											case "count" :
											case "pagecount" :
												return pageCount;

											case "date" :
											case "created" :
											case "printed" :
											case "generated" :
												return new Date().toISOString().replace( /T.+$/, "" );

											case "time" :
											case "timestamp" :
												return new Date().toISOString().replace( /T/, " " );

											default :
												return _;
										}
									} ),
								} );
							} );
						};
						break;
					}

					default :
						pdfDescription[name] = customTheme[source];
				}
			}
		}
	}


	if ( options.protect ) {
		// eslint-disable-next-line no-undef
		pdfDescription.ownerPassword = createPassword( options.rng || ( count => window.crypto.getRandomValues( new Uint16Array( count ) ) ) );
		pdfDescription.permissions = {
			printing: "lowResolution",
			modifying: false,
			copying: true,
			annotating: true,
			fillingForms: true,
			contentAccessibility: true,
			documentAssembly: true,
		};
	}

	if ( options.dumpRendered ) {
		return pdfDescription;
	}

	pdfDescription.pageBreakBefore = detectPageBreak;

	return new PdfMake( options.fonts )
		.createPdfKitDocument( pdfDescription, {
			tableLayouts: {
				boxing: {
					hLineWidth: ( i, node ) => {
						return ( node.style || {} )[i > 0 ? "borderBottom" : "borderTop"] || 0;
					},
					vLineWidth: ( i, node ) => {
						return ( node.style || {} )[i > 0 ? "borderRight" : "borderLeft"] || 0;
					},
					hLineColor: ( i, node ) => {
						const { style = {} } = node;
						return style[i > 0 ? "borderBottomColor" : "borderTopColor"] || style.borderColor || "#000000";
					},
					vLineColor: ( i, node ) => {
						const { style = {} } = node;
						return style[i > 0 ? "borderRightColor" : "borderLeftColor"] || style.borderColor || "#000000";
					},
					hLineStyle: ( i, node ) => {
						const { style = {} } = node;
						return style[i > 0 ? "borderBottomStyle" : "borderTopStyle"] || style.borderStyle || null;
					},
					vLineStyle: ( i, node ) => {
						const { style = {} } = node;
						return style[i > 0 ? "borderRightStyle" : "borderLeftStyle"] || style.borderStyle || null;
					},
					paddingTop: ( i, node ) => {
						return ( node.style || {} ).paddingTop || 0;
					},
					paddingLeft: ( i, node ) => {
						return ( node.style || {} ).paddingLeft || 0;
					},
					paddingRight: ( i, node ) => {
						return ( node.style || {} ).paddingRight || 0;
					},
					paddingBottom: ( i, node ) => {
						return ( node.style || {} ).paddingBottom || 0;
					},
					fillColor: ( i, node ) => {
						return ( node.style || {} ).backgroundColor || null;
					},
					fillOpacity: ( i, node ) => 1, // eslint-disable-line no-unused-vars
				},
			},
		} );



	/**
	 * Invokes provided callback for every node containing property `text`.
	 *
	 * @param {Definition|Definition[]} data one or more content definition nodes
	 * @param {function(Definition):void} callback callback invoked per definition node with `text`
	 * @returns {Definition|Definition[]} processed definition nodes
	 */
	function processEveryText( data, callback ) {
		const level = Array.isArray( data ) ? data : [data];
		const count = level.length;
		const copy = new Array( count );

		for ( let i = 0; i < count; i++ ) {
			const node = level[i];
			const clone = typeof node.text === "string" ? callback( node ) : Object.assign( {}, node );

			if ( Array.isArray( node.text ) ) {
				clone.text = processEveryText( node.text, callback );
			} else if ( Array.isArray( node.stack ) ) {
				clone.stack = processEveryText( node.stack, callback );
			} else if ( Array.isArray( node.columns ) ) {
				clone.columns = processEveryText( node.columns, callback );
			}

			copy[i] = clone;
		}

		return Array.isArray( data ) ? copy : copy[0];
	}


	/**
	 * Generates random password.
	 *
	 * @param {function(count:number):ArrayBuffer} rng callback implementing cryptographically strong random number generator
	 * @returns {string} generated password
	 */
	function createPassword( rng ) {
		const numbers = rng( 16 );
		const pool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-.,_:;#'+*?=)(/&%$§\"!<>";

		let password = "";
		for ( let i = 0; i < numbers.length; i++ ) {
			password += pool[numbers[i] % pool.length];
		}

		return password;
	}

	/**
	 * Compiles style names according to current set of active inline.
	 *
	 * @param {object} styles tracked names of currently applied styles
	 * @returns {string[]} style names
	 */
	function currentInlineStyles( styles ) {
		const names = Object.keys( styles ).filter( name => styles[name] );

		if ( names.length > 1 ) {
			names.sort( ( l, r ) => l.toLowerCase().localeCompare( r.toLowerCase() ) );

			names.push( names.join( "-" ).replace( /-([a-z])/g, ( _, letter ) => letter.toUpperCase() ) );
		}

		return names;
	}

	/**
	 * Converts arbitrary list of parsed tokens into list of styled text.
	 *
	 * @param {PrinterState} state context commonly available in parsing and converting markdown
	 * @param {Theme} theme definition of resulting document's theme
	 * @param {RenderingCursor} cursor cursor into tokens to process
	 * @param {string} stopAt type of token to stop at, omit for converting all tokens in list
	 * @returns {Definition[]} resulting list of styled content elements
	 */
	function convertBlock( state, theme, cursor, stopAt = undefined ) {
		const { tokens, length } = cursor;
		const items = [];
		let pageBreak = 0;

		while ( cursor.next < length ) {
			const token = tokens[cursor.next++];

			if ( stopAt && token.type === stopAt ) {
				break;
			}

			if ( token.hidden ) {
				continue;
			}

			switch ( token.type ) {
				case "paragraph_open" : {
					state.block.push( `p:${items.length + 1}` );
					const subs = convertBlock( state, theme, cursor, "paragraph_close" );
					if ( subs.length > 0 ) {
						items.push( box( pullUpInline( {
							text: subs,
							style: compileStyle( [ state.block.join( ">" ), "paragraph", "p" ], theme ),
						} ) ) );
					}
					state.block.pop();
					break;
				}

				case "bullet_list_open" : {
					state.block.push( `ul:${items.length + 1}` );
					const subs = convertList( state, theme, cursor, "bulletList", "bullet_list_close" );
					if ( subs.length > 0 ) {
						items.push( box( pullUpInline( {
							ul: subs,
							style: compileStyle( [ state.block.join( ">" ), "bulletList", "ul" ], theme ),
						} ) ) );
					}
					state.block.pop();
					break;
				}

				case "ordered_list_open" : {
					state.block.push( `ol:${items.length + 1}` );
					const subs = convertList( state, theme, cursor, "orderedList", "ordered_list_close" );
					if ( subs.length > 0 ) {
						items.push( box( pullUpInline( {
							ol: subs,
							style: compileStyle( [ state.block.join( ">" ), "orderedList", "ol" ], theme ),
						} ) ) );
					}
					state.block.pop();
					break;
				}

				case "heading_open" : {
					state.block.push( `${token.tag}:${items.length + 1}` );
					const subs = convertBlock( state, theme, cursor, "heading_close" );
					if ( subs.length > 0 ) {
						items.push( box( pullUpInline( {
							text: subs,
							style: compileStyle( [
								state.block.join( ">" ),
								"heading", `${token.tag}`,
								`heading${token.tag.replace( /^h/, "" )}`
							], theme ),
						} ) ) );
					}
					state.block.pop();
					break;
				}

				case "blockquote_open" : {
					state.block.push( `blockquote:${items.length + 1}` );
					const subs = convertBlock( state, theme, cursor, "blockquote_close" );
					if ( subs.length > 0 ) {
						items.push( box( {
							stack: subs,
							style: compileStyle( [ state.block.join( ">" ), "blockquote" ], theme ),
						}, true ) );
					}
					state.block.pop();
					break;
				}

				case "container_box_open" : {
					const containerType = token.info
						.replace( /[^a-z0-9]+/gi, " " )
						.trim()
						.toLowerCase()
						.replace( / ([a-z])/g, ( _, letter ) => letter.toUpperCase() );

					state.block.push( `${containerType}:${items.length + 1}` );
					const subs = convertBlock( state, theme, cursor, "container_box_close" );
					if ( subs.length > 0 ) {
						items.push( box( {
							stack: subs,
							style: compileStyle( [ state.block.join( ">" ), containerType, "box" ], theme ),
						}, true ) );
					}
					state.block.pop();
					break;
				}

				case "table_open" : {
					state.block.push( `table:${items.length + 1}` );
					const table = convertTable( state, theme, cursor );
					if ( table ) {
						items.push( box( table ) );
					}
					state.block.pop();
					break;
				}

				case "hr" : {
					const selectors = {
						"---": "hrDashes",
						___: "hrUnderscores",
						"***": "hrAsterisks",
					};

					const hr = { text: " ", style: compileStyle( [
						state.block.join( ">" ), "hr", selectors[token.markup]
					], theme ) };

					const wrapped = box( hr );

					if ( hr === wrapped ) {
						if ( hr.style.pageBreak || hr.style.pagebreak ) {
							pageBreak = 3;
						}
					} else {
						items.push( wrapped );
					}
					break;
				}

				case "inline" : {
					state.block.push( `inline:${items.length + 1}` );
					const children = token.children || [token.content];
					const subs = convertInline( state, theme, {
						tokens: children,
						next: 0,
						length: children.length,
					} );

					if ( subs.length > 0 ) {
						switch ( stopAt ) {
							case "td_close" :
							case "th_close" : {
								for ( let i = 0; i < subs.length; i++ ) {
									const sub = subs[i];

									if ( typeof sub.text === "string" ) {
										sub.text = sub.text.replace( /<br\s*\/?>/ig, "\n" );
									}
								}

								break;
							}
						}

						items.push( subs.length > 1 ? { text: subs } : subs[0] );
					}

					state.block.pop();
					break;
				}

				default :
					logWarning( "UNKNOWN TOKEN:", token );
					continue;
			}

			if ( pageBreak ) {
				if ( pageBreak === 1 && items.length > 0 ) {
					items[items.length - 1].pageBreak = true;
					pageBreak = 0;
				} else {
					pageBreak = pageBreak & 0xfd;
				}
			}
		}

		return items;
	}

	/**
	 * Converts parsed tokens of a list into list of styled text.
	 *
	 * @param {PrinterState} state context commonly available in parsing and converting markdown
	 * @param {Theme} theme definition of resulting document's theme
	 * @param {RenderingCursor} cursor cursor into tokens to process
	 * @param {string} listStyle style name of list
	 * @param {string} stopAt type of token to stop at
	 * @returns {Definition[]} resulting list of styled content elements
	 */
	function convertList( state, theme, cursor, listStyle, stopAt ) {
		const { tokens, length } = cursor;
		const items = [];

		while ( cursor.next < length ) {
			const token = tokens[cursor.next++];

			if ( token.type === stopAt ) {
				break;
			}

			if ( token.hidden ) {
				continue;
			}

			switch ( token.type ) {
				case "list_item_open" : {
					state.block.push( `li:${items.length + 1}` );
					const subs = convertBlock( state, theme, cursor, "list_item_close" );

					if ( subs.length > 0 ) {
						const tail = items.length % 2 === 0 ? "Odd" : "Even";
						const reduced = subs;

						items.push( {
							[Array.isArray( reduced ) ? "stack" : "text"]: reduced,
							style: compileStyle( [
								state.block.join( ">" ),
								`${listStyle}Item`, "listItem",
								`${listStyle}Item${tail}`, `listItem${tail}`,
								tail.toLowerCase()
							], theme ),
						} );
					}
					state.block.pop();
					break;
				}

				default :
					logWarning( "UNKNOWN LIST TOKEN:", token );
			}
		}

		return items;
	}

	/**
	 * Converts parsed tokens of a table into related definition.
	 *
	 * @param {PrinterState} state context commonly available in parsing and converting markdown
	 * @param {Theme} theme definition of resulting document's theme
	 * @param {RenderingCursor} cursor cursor into tokens to process
	 * @returns {Definition} definition of single table
	 */
	function convertTable( state, theme, cursor ) {
		const { tokens, length } = cursor;
		const head = [], body = [], info = { width: 0 };
		let collector;

		while ( cursor.next < length ) {
			const token = tokens[cursor.next++];

			if ( token.type === "table_close" ) {
				break;
			}

			if ( token.hidden ) {
				continue;
			}

			switch ( token.type ) {
				case "thead_open" :
					state.block.push( "thead" );
					collector = head;
					break;

				case "tbody_open" :
					state.block.push( "tbody" );
					collector = body;
					break;

				case "thead_close" :
				case "tbody_close" :
					state.block.pop();
					collector = null;
					break;

				case "tr_open" : {
					if ( !collector ) {
						throw new Error( "unexpected start of a table row due to missing container selected first" );
					}

					state.block.push( `tr:${collector.length + 1}` );
					const subs = convertTableRow( state, theme, cursor, info );

					if ( subs.length > 0 ) {
						collector.push( subs );
					}
					state.block.pop();
					break;
				}

				default :
					logWarning( "UNKNOWN TABLE TOKEN:", token );
			}
		}

		const style = compileStyle( [ state.block.join( ">" ), "table" ], theme );
		const cols = String( style.cols || "" ).trim().split( /\s*,+\s*/ ).filter( i => i !== "" );
		const widths = cols.slice( 0, info.width );

		while ( widths.length < info.width ) {
			widths.push( cols.length ? cols[cols.length - 1] : "auto" );
		}

		const table = {
			table: {
				headerRows: head.length,
				widths: widths.map( width => parseFloat( width ) || width ),
				body: head.concat( body ),
			}
		};

		if ( style ) {
			table.style = style;
		}

		if ( style.layout ) {
			table.layout = style.layout;
		}

		return table;
	}

	/**
	 * Converts parsed tokens of a table row into sequence of content definitions
	 * per cell.
	 *
	 * @param {PrinterState} state context commonly available in parsing and converting markdown
	 * @param {Theme} theme definition of resulting document's theme
	 * @param {RenderingCursor} cursor cursor into tokens to process
	 * @param {{width: int}} meta collector for additional meta information on resulting table
	 * @returns {Definition[]} resulting list of styled content elements
	 */
	function convertTableRow( state, theme, cursor, meta ) {
		const { tokens, length } = cursor;
		const cells = [];

		while ( cursor.next < length ) {
			const token = tokens[cursor.next++];

			if ( token.type === "tr_close" ) {
				break;
			}

			if ( token.hidden ) {
				continue;
			}

			switch ( token.type ) {
				case "th_open" :
				case "td_open" : {
					const cellType = token.type.substr( 0, 2 );

					state.block.push( `${cellType}:${cells.length + 1}` );
					const subs = convertBlock( state, theme, cursor, `${cellType}_close` );

					if ( subs.length > 0 ) {
						const tail = cells.length % 2 === 0 ? "Odd" : "Even";

						cells.push( {
							[Array.isArray( subs ) ? "stack" : "text"]: subs,
							style: compileStyle( [
								state.block.join( ">" ),
								cellType, `${cellType}${tail}`, tail.toLowerCase()
							], theme ),
						} );

						if ( cells.length > meta.width ) {
							meta.width = cells.length;
						}
					}

					state.block.pop();
					break;
				}

				default :
					logWarning( "UNKNOWN TABLE ROW TOKEN:", token );
			}
		}

		return cells;
	}

	/**
	 * Converts provided list of inline tokens into list of styled text.
	 *
	 * @param {PrinterState} state context commonly available in parsing and converting markdown
	 * @param {Theme} theme definition of resulting document's theme
	 * @param {RenderingCursor} cursor cursor into tokens to process
	 * @returns {Definition[]} resulting list of styled texts
	 */
	function convertInline( state, theme, cursor ) {
		const { tokens, length } = cursor;
		const chunks = [];

		while ( cursor.next < length ) {
			const token = tokens[cursor.next++];

			if ( token.hidden ) {
				continue;
			}

			switch ( token.type ) {
				case "strong_open" :
					state.inline.bold = ( state.inline.bold || 0 ) + 1;
					break;

				case "strong_close" :
					state.inline.bold = Math.max( 0, ( state.inline.bold - 1 ) || 0 );
					break;

				case "em_open" :
					state.inline.italic = ( state.inline.italic || 0 ) + 1;
					break;

				case "em_close" :
					state.inline.italic = Math.max( 0, ( state.inline.italic - 1 ) || 0 );
					break;

				case "sub_open" :
					state.inline.sub = ( state.inline.sub || 0 ) + 1;
					break;

				case "sub_close" :
					state.inline.sub = Math.max( 0, ( state.inline.sub - 1 ) || 0 );
					break;

				case "sup_open" :
					state.inline.sup = ( state.inline.sup || 0 ) + 1;
					break;

				case "sup_close" :
					state.inline.sup = Math.max( 0, ( state.inline.sup - 1 ) || 0 );
					break;

				case "s_open" :
					state.inline.strikethrough = ( state.inline.strikethrough || 0 ) + 1;
					break;

				case "s_close" :
					state.inline.strikethrough = Math.max( 0, ( state.inline.strikethrough - 1 ) || 0 );
					break;

				case "link_open" : {
					const link = attrs( token ).href;
					if ( link ) {
						state.inline.link = link;
					}
					break;
				}

				case "link_close" :
					state.inline.link = undefined;
					break;

				case "image" : {
					const src = String( attrs( token ).src || "" ).trim();

					if ( src ) {
						const sub = convertImage( state, theme, src, chunks.length + 1 );

						if ( sub ) {
							chunks.push( sub );
						}
					}
					break;
				}

				default : {
					const text = token.content === "" ? DefaultContent[token.type] : token.content;

					if ( text === undefined ) {
						if ( token.type !== "text" ) {
							logWarning( "UNKNOWN INLINE TOKEN:", token );
						}
					} else {
						const chunk = { text };
						const localStyles = currentInlineStyles( state.inline )
							.concat( token.type === "text" ? [] : [token.type] );

						const style = compileStyle( [state.block.join( ">" )].concat(
							localStyles.map( n => state.block.slice( 0, -1 ).concat( n ).join( ">" ) ), localStyles
						), theme );

						if ( style !== undefined ) {
							chunk.style = style;
						}

						if ( state.inline.link ) {
							chunk.link = state.inline.link;
						}

						chunks.push( chunk );
					}
				}
			}
		}

		return chunks;
	}

	/**
	 * Generates content definition for provided image.
	 *
	 * @param {PrinterState} state context commonly available in parsing and converting markdown
	 * @param {Theme} theme definition of resulting document's theme
	 * @param {string} imageUri URI of image to show
	 * @param {int} indexInLevel index of content element into level of resulting elements (for use with style selectors)
	 * @returns {?Definition} definition of content representing selected image
	 */
	function convertImage( state, theme, imageUri, indexInLevel ) {
		const ptnDataSvg = /^\s*(?:svg\+base64:|data:image\/svg\+xml;base64,)/i;
		let chunk;
		let type = "img";

		if ( /^\s*<svg\b|<\/svg>\s*$/i.test( imageUri ) ) {
			type = "svg";
			chunk = {
				svg: decodeURIComponent( imageUri ),
			};
		} else if ( ptnDataSvg.test( imageUri ) ) {
			type = "svg";
			chunk = {
				svg: decodeBase64( imageUri.replace( ptnDataSvg, "" ).trim() ),
			};
		} else {
			if ( !theme.images ) {
				theme.images = {};
			}

			if ( theme.images.hasOwnProperty( imageUri ) ) {
				chunk = { image: imageUri };
			} else {
				for ( const name of Object.keys( theme.images ) ) {
					if ( theme.images[name] === imageUri ) {
						chunk = { image: name };
						break;
					}
				}

				if ( !theme.images.hasOwnProperty( imageUri ) ) {
					if ( /^[a-z]+:/i.test( imageUri ) ) {
						const id = `image${indexImage++}`;

						theme.images[id] = imageUri;
						chunk = { image: id };
					}
				}
			}
		}

		if ( chunk ) {
			const style = compileStyle( [
				state.block.join( ">" ) + `>${type}:` + indexInLevel,
				type,
			], theme );

			if ( style ) {
				if ( style.width ) {
					chunk.width = style.width;
				}

				if ( style.height ) {
					chunk.height = style.height;
				}
			}

			return chunk;
		}

		logWarning( "IGNORING image without valid source" );

		return undefined;
	}

	/**
	 * Pulls up content parsed as inline to be defined as block-level content,
	 *
	 * @param {Definition} definition some definition assumed to contain inline content
	 * @returns {Definition} provided definition or its particular child with styles merged
	 */
	function pullUpInline( definition ) {
		let sub = definition.text || definition.stack || null;
		if ( !sub ) {
			return definition;
		}

		if ( Array.isArray( sub ) && sub.length === 1 ) {
			sub = sub[0];
		}

		const name = sub.svg ? "svg" : sub.image ? "image" : null;
		if ( !name ) {
			return definition;
		}


		let style;
		const theme = ( definition.style || sub.style || {} ).$theme;

		if ( theme ) {
			style = compileStyle( [
				( definition.style || {} ).$name,
				( sub.style || {} ).$name,
			], theme );
		}

		return Object.assign( {}, sub, style ? { style } : null );
	}

	/**
	 * Optionally wraps provided content in a boxing container when style is
	 * using particular styling properties requiring this wrapping.
	 *
	 * @param {Definition|Definition[]} content definition of content to wrap if required
	 * @param {boolean} force set true to enforce wrapping container
	 * @returns {Definition|Definition[]} optionally wrapped content or content as provided
	 */
	function box( content, force = false ) {
		const style = content.style;
		if ( !style ) {
			return content;
		}

		let boxing = force;

		if ( !boxing ) {
			const propNames = Object.keys( style );
			const numPropNames = propNames.length;

			for ( let i = 0; i < numPropNames; i++ ) {
				const propName = propNames[i];

				if ( BoxingProperty[propName] ) {
					boxing = true;
					break;
				}
			}
		}

		if ( boxing ) {
			const container = {
				layout: "boxing",
				table: {
					headerRows: 0,
					widths: ["*"],
					body: [
						[]
					]
				}
			};

			if ( style.columns ) {
				container.table.body[0].push( {
					columns: content.stack || [content],
				} );
			} else {
				container.table.body[0].push( content );
			}

			if ( style ) {
				container.style = style;
				content.style = undefined;
			}

			return container;
		}

		return content;
	}

	/**
	 * Parses value of named styling property.
	 *
	 * @param {*} value value to parse
	 * @param {string} property name of property this value is applied to
	 * @returns {*} parsed/normalized value
	 */
	function parseValue( value, property ) {
		switch ( property ) {
			case "margin" :
			case "border" :
			case "padding" : {
				const parsed = parseFloat( value );

				return isNaN( parsed ) ? undefined : parsed;
			}

			default :
				return undefined;
		}
	}

	/**
	 * Extracts attributes attached to given token for easier access.
	 *
	 * @param {Token} token sets of attributes
	 * @returns {object<string, string>} extracted attributes
	 */
	function attrs( token ) {
		const out = {};

		if ( Array.isArray( token.attrs ) ) {
			for ( let i = 0; i < token.attrs.length; i++ ) {
				for ( let j = 0; j < token.attrs[i].length; j += 2 ) {
					out[token.attrs[i][j]] = token.attrs[i][j + 1];
				}
			}
		}

		return out;
	}

	/**
	 * Compiles individual styling definition from selected set of styling
	 * references.
	 *
	 * @param {Array<string>} references list of names probably used in theme's stylesheet
	 * @param {Theme} theme current theme
	 * @returns {?CompiledStylesheet} individual stylesheet matching all provided selectors
	 */
	function compileStyle( references, theme ) {
		const { styles } = theme;
		const numReferences = references.length;
		const picked = [];
		const hierarchical = [];

		// collect names of all directly addressed style definitions
		for ( let i = 0; i < numReferences; i++ ) {
			const reference = String( references[i] || "" ).trim();

			if ( /[\s>:]/.test( reference ) ) {
				hierarchical.push( reference );
			} else if ( theme.styles[reference] ) {
				picked.push( {
					name: reference,
					index: picked.length,
					specificity: 0,
				} );
			}
		}

		// try complex selectors in stylesheet for matching any provided
		// hierarchical style reference
		if ( hierarchical.length > 0 ) {
			const styleNames = Object.keys( styles );
			const numStyleNames = styleNames.length;

			for ( let i = 0; i < numStyleNames; i++ ) {
				const styleName = styleNames[i];
				const style = styles[styleName];

				if ( !style.hasOwnProperty( "$selector" ) ) {
					Object.defineProperty( style, "$selector", { value: compileSelector( styleName ) } );
				}

				if ( style.$selector ) {
					let bestSpecificity = -1;

					for ( let j = 0; j < numReferences; j++ ) {
						const ref = references[j];

						if ( style.$selector.test( ref ) ) {
							const specificity = ( ref.replace( /[^>]/g, "" ).length * 100 ) + ref.split( /[ >]/ ).length;

							if ( specificity > bestSpecificity ) {
								bestSpecificity = specificity;
							}
						}
					}

					if ( bestSpecificity > -1 ) {
						picked.push( {
							name: styleName,
							index: picked.length,
							specificity: bestSpecificity
						} );
					}
				}
			}
		}

		if ( picked.length < 1 ) {
			return undefined;
		}

		// sort all collected names of style definitions to apply
		// (from generic to specific styles while keeping order of references as good as possible)
		picked.sort( ( l, r ) => ( l.specificity - r.specificity ) || l.index - r.index );

		// compile style unless being compiled before
		const id = picked.map( ( { name } ) => name ).join( "$" );
		if ( !( theme.compiled || {} )[id] ) {
			const compiled = ( theme.compiled = theme.compiled || {} )[id] = {};
			const numPicked = picked.length;

			for ( let i = 0; i < numPicked; i++ ) {
				const name = picked[i].name;
				const style = name === "__default" ? DefaultStyle : styles[name];
				const props = Object.keys( style ).sort( ( l, r ) => l.localeCompare( r ) );
				const numProps = props.length;

				for ( let j = 0; j < numProps; j++ ) {
					const prop = props[j];
					let value = style[prop];
					let multiValue = false;

					switch ( prop ) {
						case "marginTop" :
						case "marginRight" :
						case "marginBottom" :
						case "marginLeft" :
						case "borderTop" :
						case "borderRight" :
						case "borderBottom" :
						case "borderLeft" :
						case "paddingTop" :
						case "paddingRight" :
						case "paddingBottom" :
						case "paddingLeft" : {
							value = parseValue( value, BaseMap[prop] );
							break;
						}

						case "margin" :
						case "border" :
						case "padding" : {
							multiValue = true;

							if ( Array.isArray( value ) ) {
								switch ( value.length ) {
									case 0 :
										value = undefined;
										break;

									case 1 : {
										const parsed = parseValue( value[0], prop );
										value = [ parsed, parsed, parsed, parsed ];
										break;
									}

									case 2 : {
										const vertical = parseValue( value[0], prop );
										const horizontal = parseValue( value[1], prop );
										value = [ vertical, horizontal, vertical, horizontal ];
										break;
									}

									case 3 : {
										const right = parseValue( value[1], prop );
										value = [ parseValue( value[0], prop ), right, parseValue( value[2], prop ), right ];
										break;
									}

									default :
										value[0] = parseValue( value[0], prop );
										value[1] = parseValue( value[1], prop );
										value[2] = parseValue( value[2], prop );
										value[3] = parseValue( value[3], prop );
								}
							} else {
								const parsed = parseValue( value, prop );
								value = [ parsed, parsed, parsed, parsed ];
							}
							break;
						}
					}

					if ( multiValue && Array.isArray( value ) ) {
						for ( let k = 0; k < value.length; k++ ) {
							if ( value[k] !== undefined ) {
								compiled[prop + DirectionNames[k]] = value[k];
							}
						}
					} else if ( value !== undefined ) {
						compiled[prop] = value;
					}
				}
			}

			Object.defineProperties( compiled, {
				$name: { value: id },
				$theme: { value: theme },
				toString: { value: () => id },
			} );
		}

		return theme.compiled[id];
	}

	/**
	 * Tries to compile regular expression for provided selector to match
	 * hierarchical style references generated in content definition.
	 *
	 * @param {string} selector selector to compile, e.g. used in stylesheet
	 * @returns {RegExp} regular expression suitable for matching style references of content nodes
	 */
	function compileSelector( selector ) {
		const normalized = String( selector ).trim()
			.replace( /\s+/g, " " )
			.replace( / *> */g, ">" );

		if ( /^(?:[a-z0-9_]+(?::\d+)?[ >])+[a-z0-9_]+(?::\d+)?$/i.test( normalized ) ) {
			const segments = normalized.split( /([ >])/ );

			for ( let j = 0; j < segments.length; j++ ) {
				if ( j % 2 ) {
					if ( segments[j] === " " ) {
						segments[j] = "\\b.+\\b";
					}
				} else if ( !/:\d+$/.test( segments[j] ) ) {
					segments[j] += "(?::\\d+)?";
				}
			}

			return new RegExp( `\\b${segments.join( "" )}$`, "i" );
		}

		return undefined;
	}

	/**
	 * Extracts raw (string) data from provided base64-encoded string.
	 *
	 * @param {string} encoded base64-encoded string
	 * @returns {string} decoded string
	 */
	function decodeBase64( encoded ) {
		if ( typeof atob !== "undefined" ) {
			return ( decoded => {
				const numBytes = decoded.length;
				const bytes = new Uint8Array( numBytes );

				for ( let i = 0; i < numBytes; i++ ) {
					bytes[i] = decoded.charCodeAt( i );
				}

				return String.fromCharCode( ...new Uint16Array( bytes.buffer ) );
			} )( atob( encoded ) ); // eslint-disable-line no-undef
		} else if ( typeof Buffer !== "undefined" ) {
			return Buffer.from( encoded, "base64" ).toString( "utf8" );
		}

		throw new Error( "invalid environment: atob() or Buffer() required" );
	}

	/**
	 * Detects whether page break should be inserted before provided node or not.
	 *
	 * @param {Definition} currentNode refers to current node for inspection
	 * @param {Definition[]} onSamePage lists nodes currently succeeding on current page
	 * @param {Definition[]} onNextPage lists nodes currently succeeding on next page
	 * @param {Definition[]} predecessors lists nodes currently preceding on current page
	 * @returns {boolean} true if page break should be inserted before selected node
	 */
	function detectPageBreak( currentNode, onSamePage, onNextPage, predecessors ) { // eslint-disable-line no-unused-vars
		const { style = {} } = currentNode;

		return currentNode.pageBreak || style.pageBreak || style.pagebreak;
	}
};


/**
 * @typedef {object} TokenRendererOptions
 * @property {Theme} theme definition of theme to apply
 * @property {function(string, ...):void} logWarning callback for logging warnings
 * @property {boolean} protect set true to create PDF with limited access
 * @property {function(count:number):ArrayBuffer} rng callback for generating cryptographically strong sequence of random values
 * @property {boolean} dumpRendered set true to get description of resulting PDF instead of actual document
 * @property {object<string,object<string,string>>} fonts maps font name into map of font face names into either face's local file name (server-side, only)
 */

/**
 * @typedef {object} Token
 * @property {string} type type name of token
 * @property {boolean} hidden indicates if token is meant to be excluded from resulting output
 * @property {string} tag suggested name of tag to use in generating HTML output
 * @property {string} [content] simple text content
 * @property {string} info extra information on some types of tokens
 * @property {string} markup markup source of current token
 * @property {Array<Array<string>>} [attrs] attributes customizing token
 * @property {Token[]} [children] lists subordinated tokens (used for inline tokens in context of single block token
 */

/**
 * @typedef {object} CommonDefinition
 * @property {CompiledStylesheet} style styling properties to apply on current node
 * @property {boolean} pageBreak optional indicator requesting to start new page before current content
 */

/**
 * @typedef {CommonDefinition} TextDefinition
 * @property {string|Array<string>|Array<Definition>} text describes content of chunk
 */

/**
 * @typedef {CommonDefinition} UnorderedListDefinition
 * @property {string|Array<string>|Array<Definition>} ul describes items of list
 */

/**
 * @typedef {CommonDefinition} OrderedListDefinition
 * @property {string|Array<string>|Array<Definition>} ol describes items of list
 */

/**
 * @typedef {TextDefinition|UnorderedListDefinition|OrderedListDefinition} Definition
 */

/**
 * @typedef {object} RendererState
 * @property {string[]} block tracks breadcrumb of block-level styles applicable on current element
 * @property {object<string, number>} inline tracks enabled inline-level styles to apply on inline elements
 */

/**
 * @typedef {RendererState} PrinterState
 * @property {object} env environment used by parser for collecting side-band information
 * @property {Array<RendererState>} stack LIFO queue for temporarily saving/replacing current state
 */

/**
 * @typedef {object} RenderingCursor
 * @property {Token[]} tokens sequence of tokens to render
 * @property {int} next index of next token to render
 */

/**
 * @typedef {object} Theme
 * @property {object} styles dictionary of styles, @see https://pdfmake.github.io/docs/document-definition-object/styling/#style-dictionaries
 * @property {object} defaultStyle default style, @see https://pdfmake.github.io/docs/document-definition-object/styling/#default-style
 * @property {object} [header] definition of page header, @see https://pdfmake.github.io/docs/document-definition-object/headers-footers/
 * @property {object} [footer] definition of page footer, @see https://pdfmake.github.io/docs/document-definition-object/headers-footers/
 * @property {object<string, object>} [compiled] dictionary of compiled styles
 * @property {object<string, string>} [images] dictionary of reusable images' URLs
 */

/**
 * @typedef {object} CompiledStylesheet
 * @property {string} $name exposes name of stylesheet in set of theme's compiled stylesheets
 * @property {Theme} $theme refers to current theme this style is contained in
 */

/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Markdown = require( "markdown-it" );

/**
 * @typedef {object} MarkdownProcessorOptions
 * @property {boolean} render set true to get rendered HTML as string instead of tokens and parser state for custom post-processing
 * @property {boolean} typographer controls whether typographer feature of markdown parser is enabled (true by default)
 * @property {string} quotes lists pairs of custom quotes to use using any other pair of quotes when encountering nested quotes
 */

/**
 * Implements common processing of markdown input.
 *
 * @note This method has been extracted to expose internally used markdown
 *       parser to consuming libraries for custom use.
 *
 * @param {string} input markdown content to be parsed/converted.
 * @param {object} options customizations
 * @returns {string|{tokens: *, state: {stack: [], inline: {}, block: [], env: {}}}} rendered HTML or resulting tokens and state of parser
 */
exports.process = ( input, options = {} ) => {
	const engine = Markdown( {
		typographer: options.typographer !== false,
		quotes: options.quotes, // || "„“‚‘",
	} )
		.use( require( "markdown-it-abbr" ) )
		.use( require( "markdown-it-container" ), "box" )
		.use( require( "markdown-it-deflist" ) )
		.use( require( "markdown-it-emoji" ) )
		.use( require( "markdown-it-footnote" ) )
		.use( require( "markdown-it-ins" ) )
		.use( require( "markdown-it-mark" ) )
		.use( require( "markdown-it-sub" ) )
		.use( require( "markdown-it-sup" ) );

	const state = {
		env: {},
		block: [],
		inline: {},
		stack: [],
	};

	if ( options.render ) {
		return engine.render( input );
	}

	return {
		tokens: engine.parse( input, state.env ),
		state,
	};
};
